package com.connector;

import com.dto.ProductDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Component
public class ProductConnector {
    private static final String BASE_URL = "http://localhost:8080";
    private final RestTemplate restTemplate = new RestTemplate();


    public List<ProductDTO> getProductList() {

        return restTemplate.exchange(
                BASE_URL + "/allProducts",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductDTO>>() {
                }
        ).getBody();
    }

    public ProductDTO getProductById(UUID productId) {
        return restTemplate.exchange(
                BASE_URL + "/product?id=" + productId,
                HttpMethod.GET,
                null,
                ProductDTO.class
        ).getBody();
    }

    public void deleteProductById(UUID id) {
        restTemplate.delete(BASE_URL + "/deleteProduct?id=" + id);
    }

    public void addProduct(ProductDTO product) {
        restTemplate.postForObject(BASE_URL + "/addProduct", product, ProductDTO.class);
    }

    public void updateProduct(ProductDTO product) {
        restTemplate.put(BASE_URL + "/updateProduct", product);
    }
}
