package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontEndInventoryManageEvaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FrontEndInventoryManageEvaApplication.class, args);
    }

}
