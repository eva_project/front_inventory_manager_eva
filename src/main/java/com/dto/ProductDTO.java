package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ProductDTO {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("productDescription")
    private String productDescription;

    @JsonProperty("countryOfManufacture")
    private String countryOfManufacture;

    @JsonProperty("price")
    private BigDecimal price;

    @JsonProperty("quantityInStock")
    private Long quantityInStock;
}

