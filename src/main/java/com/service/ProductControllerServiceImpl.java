package com.service;

import com.connector.ProductConnector;
import com.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductControllerServiceImpl implements ProductControllerService {
    private final ProductConnector productConnector;

    @Autowired
    public ProductControllerServiceImpl(ProductConnector productConnector) {
        this.productConnector = productConnector;
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        return productConnector.getProductList();
    }


    @Override
    public ProductDTO getProductById(UUID productId) {
        return productConnector.getProductById(productId);
    }

    @Override
    public void deleteProductById(UUID id) {
        productConnector.deleteProductById(id);
    }

    @Override
    public void addProduct(ProductDTO product) {
        productConnector.addProduct(product);
    }

    @Override
    public void updateProduct(ProductDTO product) {
        productConnector.updateProduct(product);
    }
}
