package com.service;

import com.dto.ProductDTO;

import java.util.List;
import java.util.UUID;

public interface ProductControllerService {
    List<ProductDTO> getAllProducts();

    ProductDTO getProductById(UUID productId);

    void deleteProductById(UUID id);

    void addProduct(ProductDTO product);

    void updateProduct(ProductDTO product);
}
