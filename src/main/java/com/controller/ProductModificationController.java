package com.controller;

import com.dto.ProductDTO;
import com.service.ProductControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@Controller
public class ProductModificationController {

    private ProductControllerService productControllerService;

    @Autowired
    public ProductModificationController(@Qualifier("productControllerServiceImpl") ProductControllerService productControllerService) {
        this.productControllerService = productControllerService;
    }

    @PostMapping("/addProduct")
    public String addProduct(@ModelAttribute ProductDTO product) {
        productControllerService.addProduct(product);

        return "redirect:/";
    }

    @PostMapping("/updateProduct")
    public String updateProduct(@ModelAttribute ProductDTO product) {
        productControllerService.updateProduct(product);

        return "redirect:/";
    }

    @PostMapping("/deleteProduct/{productId}")
    public String deleteProductById(@PathVariable("productId") UUID id) {
        productControllerService.deleteProductById(id);

        return "redirect:/";
    }
}
