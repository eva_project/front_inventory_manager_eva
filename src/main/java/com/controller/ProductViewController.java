package com.controller;

import com.dto.ProductDTO;
import com.service.ProductControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
public class ProductViewController {

    private ProductControllerService productControllerService;

    @Autowired
    public ProductViewController(@Qualifier("productControllerServiceImpl") ProductControllerService productControllerService) {
        this.productControllerService = productControllerService;
    }

    @GetMapping("/")
    public String getAllProducts(Model model) {
        List<ProductDTO> productDTOList = productControllerService.getAllProducts();
        model.addAttribute("productList", productDTOList);

        return "product/list";
    }

    @GetMapping("/product/{productId}")
    public String getProductById(@PathVariable("productId") String id, Model model) {
        UUID productId = UUID.fromString(id);
        ProductDTO productDTO = productControllerService.getProductById(productId);
        model.addAttribute("product", productDTO);

        return "product/view";
    }

    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        model.addAttribute("product", new ProductDTO());

        return "product/add";
    }

    @GetMapping("/updateProduct/{productId}")
    public String updateProduct(@PathVariable("productId") String id, Model model) {
        UUID productId = UUID.fromString(id);
        ProductDTO product = productControllerService.getProductById(productId);
        model.addAttribute("product", product);

        return "product/upd";
    }
}
